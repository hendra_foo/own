require('./bootstrap');
window.Vue = require('vue');
// import Vue from 'vue';
import VueInternationalization from 'vue-i18n';
import Locale from './vue-i18n-locales.generated';
import VueRouter from 'vue-router';
import router from './routes.js';
import Home from './components/Home.vue';

Vue.use(VueInternationalization);
Vue.use(VueRouter);
const lang = document.documentElement.lang.substr(0, 2); 
const i18n = new VueInternationalization({
    locale: lang,
    messages: Locale
});

const app = new Vue({
    el: '#app',
    i18n,
    router,
    component: {
        Home,
    },
    render: h => h(Home)
});
