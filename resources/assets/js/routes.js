import VueRouter from 'vue-router'
import DashboardComponent from './components/Dashboard/DashboardComponent.vue';

export default new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    { 
        path: '/',
        name: 'dashboard',
        component: DashboardComponent,
    },
  ]
})