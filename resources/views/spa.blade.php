<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=1024, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
        <title>Own</title>
	    <link rel="stylesheet" type="text/css" href="{{ mix('/css/app.css') }}"/>
	    <link rel="stylesheet" type="text/css" href="{{ asset('/css/styles.css') }}"/>
    </head>
    <body>
        <div id="app"></div>
    </body>
    <script type="text/javascript" src="{{ mix('/js/app.js') }}"></script>
</html>
